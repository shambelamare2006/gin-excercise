package main

import (
	"log"

	"github.com/gin-gonic/gin"
	controller "github.com/shambel-amare/gin-framework/gin-excercise/controllers"
	"github.com/shambel-amare/gin-framework/gin-excercise/middlewares"
	service "github.com/shambel-amare/gin-framework/gin-excercise/services"
)

var (
	videoService    service.VideoService       = service.New()
	videoController controller.VideoController = controller.New(videoService)
)

const port = ":8080"

func main() {
	server := gin.Default()
	// server.Static("/css", "./templates/css")
	server.LoadHTMLGlob("templates/*.html")
	// server.Use(gin.Recovery(), middlewares.Logger(), middlewares.Auth(), middlewares.Check())
	apiRoutes := server.Group("api/videos", middlewares.Auth())
	{
		apiRoutes.GET("/list", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.FindAll())
		})
		apiRoutes.POST("/post", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.Save(ctx))
		})
	}
	viewRoutes := server.Group("/view")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)
	}
	server.Run(port)
	log.Printf("Listning on port:%v", port)
}
