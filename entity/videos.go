package entity

type Person struct {
	FirstName string `json:"firstname" binding:"required"`
	LastName  string `json:"lastname" binding:"required"`
	Age       int    `json:"age" binding:"gte=1,lte=120"`
	Email     string `json:"email" validate:"is_email"`
}
type Video struct {
	Title       string `json:"title" binding:"min=2,max=20"`
	Description string `json:"description" binding:"max=30"`
	URL         string `json:"url" validate:"required"`
	Author      Person `json:"author" binding:"required"`
}
