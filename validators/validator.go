package validators

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

// isEmailValid checks if the email provided is valid by regex.
func IsEmailValid(field validator.FieldLevel) bool {
	emailRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return emailRegex.MatchString(field.Field().String())
}
