package middlewares

import (
	"log"

	"github.com/gin-gonic/gin"
)

func Check() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		log.Println(ctx.MustGet("Shambel"))
	}
}
